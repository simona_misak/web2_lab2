import * as he from "he";

interface customWindow extends Window {
	parseParams?: any;
}

declare const window: customWindow;

function parseParams() {
	const queryString = window.location.search;

	const urlParams = new URLSearchParams(queryString);

	const enableVulnerability = urlParams.get('enableVulnerability') == 'on';

	const searchText = urlParams.get('dataInput');
	const text = enableVulnerability ? searchText : searchText ? he.encode(searchText) : "";

	console.log(text);

	document.body.innerHTML += '<p>Search Input: ' + text + '</p>';
}

window.onload = parseParams;