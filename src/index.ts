import express from 'express';
import path from 'path'
import http from 'http';
import dotenv from 'dotenv'
import { encode } from 'he';
import { Pool } from 'pg'
dotenv.config()

const app = express();
app.set("views", path.join(__dirname, "views"));
app.use(express.static(path.join(__dirname, "views")));

const pool = new Pool({
	user: process.env.DB_USER,
	host: process.env.DB_HOST,
	database: 'web2_lab2_db_1cez',
	password: process.env.DB_PASSWORD,
	port: 5432,
	ssl : {rejectUnauthorized: false}
	})

const externalUrl = process.env.RENDER_EXTERNAL_URL;
const port = externalUrl && process.env.PORT ? parseInt(process.env.PORT) : 4080;

app.get('/', function (req, res) {
  res.render('index', {});
});

app.get('/xss', async function (req, res) {
	const enableVulnerability = req.query.enableVulnerability == 'on';

	const searchText = req.query.dataInput as string || '';
	const input = enableVulnerability ? searchText : searchText ? encode(searchText) : "";

	res.send(`
		<!DOCTYPE html>
		<html lang="en">
		<head>
				<meta charset="UTF-8">
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
				<title>Cross-Site Scripting</title>
		</head>
		<body>
				<h1>Search Results</h1>

				<label for="dynamicContent">Input: </label>
				<div id="dynamicContent">
						${input}
				</div>

				<br> </br>
		</body>
		</html>
		`);
});

app.get('/sde', async function (req, res) {
	const enableVulnerability = req.query.enableSensitiveData == 'on';

	const searchText = req.query.sensitiveDataInput as string || '';
	const input = searchText ? encode(searchText) : "";

	const userList = enableVulnerability ? await generateUserTable(input) : await generateSensitiveUserTable(input);

	res.send(`
		<!DOCTYPE html>
		<html lang="en">
		<head>
				<meta charset="UTF-8">
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
				<title>Cross-Site Scripting</title>
		</head>
		<body>
				<h1>Search Results</h1>

				<label for="dynamicContent">Input: </label>
				<div id="dynamicContent">
						${input}
				</div>

				<br> </br>

				<div id="searchResults">
						${userList}
				</div>
		</body>
		</html>
		`);
});


if (externalUrl) {
	const hostname =
	'0.0.0.0'; //ne 127.0.0.1
	app.listen(port, hostname, () => {
		console.log(`Server locally running at http://${hostname}:${port}/ and from outside on ${externalUrl}`);
	});
} else {
	http.createServer(app)
	.listen(port, function () {
	console.log(`Server running at http://localhost:${port}/`);
	});
}


async function generateUserList(input: string) {
	const users = await getUsers(input);

	if (!users || users.length === 0) {
			return '<p>No users found.</p>';
	}

	const userList = users.map(user => {
			return `<li>${user.first_name} ${user.last_name} - ${user.email}</li>`;
	}).join('');

	return `<ul>${userList}</ul>`;
}

async function generateUserTable(input: string) {
	const users = await getUsers(input);;

	if (!users || users.length === 0) {
			return generateNoUsersFound();
	}

	const tableHeaders = '<tr><th>ID</th><th>First Name</th><th>Last Name</th><th>Email</th><th>Age</th><th>City</th><th>Country</th><th>Phone Number</th></tr>';
	
	const userRows = users.map((user: any) => {
			return `<tr>
									<td>${user.id}</td>
									<td>${user.first_name}</td>
									<td>${user.last_name}</td>
									<td>${user.email}</td>
									<td>${user.age}</td>
									<td>${user.city}</td>
									<td>${user.country}</td>
									<td>${user.phone_number}</td>
							</tr>`;
	}).join('');

	return `<table border="1">${tableHeaders}${userRows}</table>`;
};

function generateNoUsersFound() {
	return '<p>No users found.</p>';
}

async function generateSensitiveUserTable(input: string) {
	const users = await getUsers(input);

	if (!users || users.length === 0) {
			return '<p>No users found.</p>';
	}

	const tableHeaders = '<tr><th>ID</th><th>First Name</th><th>Last Name</th><th>Email</th><th>Age</th><th>Country</th></tr>';
	
	const userRows = users.map((user: any) => {
			return `<tr>
									<td>${user.id}</td>
									<td>${user.first_name}</td>
									<td>${user.last_name}</td>
									<td>${user.email}</td>
									<td>${user.age}</td>
									<td>${user.country}</td>
							</tr>`;
	}).join('');

	return `<table border="1">${tableHeaders}${userRows}</table>`;
};

export async function getUsers(input: string) {

	const users : any[] = [];
	const sqlQuery = `
    SELECT *
    FROM users
    WHERE LOWER(first_name) LIKE LOWER('%${input}%') OR LOWER(last_name) LIKE LOWER('%${input}%');
		`;
	
	try {
		const results = await pool.query(sqlQuery);

		results.rows.forEach(r => {
			users.push(r);
		});
		return users;
	} catch(ex) {
		return null;
	}
}
